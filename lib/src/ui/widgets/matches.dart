import 'package:flutter/widgets.dart';

class MatchEngine extends ChangeNotifier {
  final List<Match> _matches;
  int _currrentMatchIndex;
  int _nextMatchIndex;
  int index;

  MatchEngine({
    List<Match> matches,
  }) : _matches = matches {
    _currrentMatchIndex = 0;
    _nextMatchIndex = 1;
  }

  Match get currentMatch => _matches[_currrentMatchIndex];
  Match get nextMatch => _matches[_nextMatchIndex];

  void cycleMatch() {
    if (currentMatch.decision != Decision.indecided) {
      currentMatch.reset();
      _currrentMatchIndex = _nextMatchIndex;
      _nextMatchIndex =
          _nextMatchIndex < _matches.length - 1 ? _nextMatchIndex + 1 : 0;
      notifyListeners();
    } else {
      print(1);
    }
    print(_matches.length.toString());
    print(_currrentMatchIndex.toString());
    print(_nextMatchIndex.toString());
  }

  void prevImage() {
    index = _currrentMatchIndex;
    if (index > 0) {
      _currrentMatchIndex = _currrentMatchIndex - 1;
      if (_currrentMatchIndex > 0) {
        _nextMatchIndex = _currrentMatchIndex + 1;
      }
    }
    notifyListeners();
  }

  void printIndex() {
    print(_currrentMatchIndex);
  }
}

class Match extends ChangeNotifier {
  final profile;
  var decision = Decision.indecided;

  Match({this.profile, this.decision});

  void like() {
    //  if (decision == Decision.indecided) {
    decision = Decision.like;
    notifyListeners();
    // }
  }

  void nope() {
    // if (decision == Decision.indecided) {
    decision = Decision.nope;
    notifyListeners();
    //  }
  }

  void superLike() {
    // if (decision == Decision.indecided) {
    decision = Decision.superLike;
    notifyListeners();
    // }
  }

  void reset() {
    // if (decision != Decision.indecided) {
    decision = Decision.indecided;
    notifyListeners();
    // }
  }
}

enum Decision {
  indecided,
  nope,
  like,
  superLike,
}
