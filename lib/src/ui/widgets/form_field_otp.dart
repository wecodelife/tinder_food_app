import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildOtp extends StatefulWidget {
  final String labelText;
  final bool otp;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  FormFeildOtp({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
    this.otp,
  });
  @override
  _FormFeildOtpState createState() => _FormFeildOtpState();
}

class _FormFeildOtpState extends State<FormFeildOtp> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 16),
        child: TextField(
          cursorColor: Constants.kitGradients[1],
          obscureText: false,
          keyboardType: widget.otp ? TextInputType.number : TextInputType.text,
          style: TextStyle(
            color: Constants.kitGradients[1],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
          decoration: InputDecoration(
            labelText: widget.labelText,
            labelStyle: TextStyle(
              color: Constants.kitGradients[1],
              fontFamily: 'OpenSansRegular',
              fontSize: 16,
            ),
            fillColor: Colors.white,

            focusedBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Constants.kitGradients[1], width: 0.0),
              borderRadius: BorderRadius.circular(20.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Constants.kitGradients[1], width: 0.0),
              borderRadius: BorderRadius.circular(20.0),
            ),
            // enabledBorder: InputBorder(
            //    BorderSide(color: Constants.kitGradients[29], width: 1.0),
            //   borderRadius: BorderRadius.circular(10.0),
            // )
            // UnderlineInputBorder(
            //   borderSide: BorderSide(color: Constants.kitGradients[29]),
            // ),
          ),
        ));
  }
}
