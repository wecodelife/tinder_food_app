import 'package:flutter/material.dart';

class PriceRatingBar extends StatefulWidget {
  final int price;
  final double size;
  PriceRatingBar({this.price, this.size});
  @override
  _PriceRatingBarState createState() => _PriceRatingBarState();
}

Widget _buildDollars(
  BuildContext context,
  int price,
  double size,
) {
  String dolls = "\u20B9";
  Color iColor = Colors.white;

  return Text(
    price.toString() + " \u20B9",
    style:
        TextStyle(color: iColor, fontWeight: FontWeight.bold, fontSize: size),
  );
}

class _PriceRatingBarState extends State<PriceRatingBar> {
  @override
  Widget build(BuildContext context) {
    return _buildDollars(context, widget.price, widget.size);
  }
}
