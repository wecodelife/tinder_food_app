import 'package:app_template/src/ui/widgets/food_detail_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom/pinch_zoom.dart';

class FoodInformationPage extends StatefulWidget {
  final Widget navigationWidget;
  FoodInformationPage({this.navigationWidget});
  @override
  _FoodInformationPageState createState() => _FoodInformationPageState();
}

class _FoodInformationPageState extends State<FoodInformationPage> {
  List<String> imgList = [
    "assets/images/cafe.jpg",
    "assets/images/gelato.jpg",
    "assets/images/tacos.jpg",
  ];
  int index = 0;
  bool reverse = false;
  Future<bool> _willPopCallback() async {
    pushAndReplacement(context, widget.navigationWidget);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              GestureDetector(
                onTap: () {
                  if (index < imgList.length - 1 && reverse == false) {
                    setState(() {
                      index = index + 1;
                    });
                  } else {
                    if (index == 1) {
                      reverse = false;
                    } else {
                      reverse = true;
                    }
                    setState(() {
                      index = index - 1;
                    });
                  }
                },
                child: Container(
                  height: screenHeight(context, dividedBy: 1),
                  decoration: BoxDecoration(),
                  child: PinchZoom(
                    image: Image(
                      image: AssetImage(imgList[index]),
                      fit: BoxFit.cover,
                    ),
                    zoomedBackgroundColor: Colors.black.withOpacity(0.5),
                    resetDuration: const Duration(milliseconds: 100),
                    maxScale: 2.5,
                    onZoomStart: () {},
                    onZoomEnd: () {},
                  ),
                ),
              ),
              Positioned(
                top: screenHeight(context, dividedBy: 17),
                left: screenWidth(context, dividedBy: 30),
                child: Container(
                  height: screenHeight(context, dividedBy: 21),
                  width: screenWidth(context, dividedBy: 9),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(0.5),
                  ),
                  child: IconButton(
                    icon: Icon(Icons.close, color: Colors.white),
                    onPressed: () {
                      pushAndReplacement(context, widget.navigationWidget);
                    },
                    iconSize: 20.0,
                  ),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 2.1),
                  ),
                  Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[0],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30))),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20)),
                        child: Column(
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 40),
                            ),
                            Row(
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 1.5),
                                  child: Text(
                                    "Biriyani Rahamath",
                                    style: TextStyle(
                                        color: Constants.kitGradients[2],
                                        fontWeight: FontWeight.w800,
                                        fontSize: 22,
                                        fontFamily: 'OpenSansLight'),
                                  ),
                                ),
                                FoodLocationWidget(
                                  latitude: 9.9312,
                                  longitude: 76.2673,
                                ),
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            Row(
                              children: [
                                Text(
                                  "DESCRIPTION",
                                  style: TextStyle(
                                      color: Constants.kitGradients[2],
                                      fontWeight: FontWeight.w800,
                                      fontSize: 22,
                                      fontFamily: 'OswaldRegular'),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical:
                                      screenHeight(context, dividedBy: 50)),
                              child: Text(
                                "A world-renowned Indian dish, biryani takes time and practice to make but is worth every bit of the effort. Long-grained rice (like basmati) flavored with fragrant spices such as saffron and layered with lamb, chicken, fish, or vegetables and a thick gravy. The dish is then covered, its lid secured with dough, and then the biryani is "
                                "cooked over a low flame. This is definitely a special occasion dish.",
                                style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 23),
                                  fontFamily: 'OpenSansRegular',
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  "RECIPE",
                                  style: TextStyle(
                                      color: Constants.kitGradients[2],
                                      fontWeight: FontWeight.w800,
                                      fontSize: 22,
                                      fontFamily: 'OswaldRegular'),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 50),
                            ),
                            ListView.builder(
                                itemCount: 2,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.zero,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    height:
                                        screenHeight(context, dividedBy: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: screenWidth(context,
                                              dividedBy: 25),
                                          decoration: BoxDecoration(
                                              color: Colors.black,
                                              shape: BoxShape.circle),
                                        ),
                                        SizedBox(
                                          width: screenWidth(context,
                                              dividedBy: 60),
                                        ),
                                        Text(
                                          "Recipe",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 20),
                            )
                          ],
                        ),
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
