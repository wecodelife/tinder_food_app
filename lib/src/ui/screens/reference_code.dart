import 'package:app_template/src/ui/screens/tinder_card.dart';
import 'package:app_template/src/ui/widgets/app_bar.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_field_password.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReferenceCodePage extends StatefulWidget {
  @override
  _ReferenceCodePageState createState() => _ReferenceCodePageState();
}

class _ReferenceCodePageState extends State<ReferenceCodePage> {
  TextEditingController referenceCodeTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [
          AppBarFoodApp(
            title: "ReferenceCode",
            leftIconTrue: true,
            leftIcon: Icons.arrow_back,
            onPressedLeftIcon: () {
              pop(context);
            },
          )
        ],
      ),
      body: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 2.9),
          ),
          SizedBox(
            height: 10,
          ),
          FormFeildPasswordBox(
            textEditingController: referenceCodeTextEditingController,
            hintText: "ReferenceCode",
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3),
          ),
          BuildButton(
            title: "NEXT",
            onPressed: () {
              push(context, TinderCard());
            },
          )
        ],
      ),
    );
  }
}
