import 'package:app_template/src/ui/screens/tinder_card.dart';
import 'package:app_template/src/ui/widgets/form_field_otp.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OTPInputPage extends StatefulWidget {
  @override
  _OTPInputPageState createState() => _OTPInputPageState();
}

class _OTPInputPageState extends State<OTPInputPage> {
  TextEditingController otpTextEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.black,
            body: SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 3),
                    ),
                    FormFeildOtp(
                      labelText: "Enter your OTP",
                      textEditingController: otpTextEditingController,
                      otp: true,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    GestureDetector(
                      onTap: () {
                        //push(context, LoginPage());
                      },
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Resend OTP",
                          style: TextStyle(
                            color: Constants.kitGradients[1],
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 2.5),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        color: Constants.kitGradients[1],
                        child: Center(
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        onPressed: () {
                          push(context, TinderCard());
                        },
                      ),
                    ),
                  ])),
            )));
  }
}
