import 'package:app_template/src/ui/screens/food_information_page.dart';
import 'package:app_template/src/ui/screens/tinder_card.dart';
import 'package:app_template/src/ui/widgets/app_bar.dart';
import 'package:app_template/src/ui/widgets/favourites_box.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FavouritesPage extends StatefulWidget {
  @override
  _FavouritesPageState createState() => _FavouritesPageState();
}

class _FavouritesPageState extends State<FavouritesPage> {
  @override
  Future<bool> _willPopCallback() async {
    pushAndReplacement(context, TinderCard());
  }

  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          appBar: AppBar(
            leading: Container(),
            backgroundColor: Colors.black,
            actions: [
              AppBarFoodApp(
                title: "FAVOURITES",
                onPressedLeftIcon: () {
                  pushAndReplacement(context, TinderCard());
                },
                leftIcon: Icons.arrow_back,
                leftIconTrue: true,
              )
            ],
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: ListView.builder(
                        padding: EdgeInsets.zero,
                        itemCount: 6,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return index % 2 == 0
                              ? Column(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: GestureDetector(
                                        onTap: () {
                                          push(
                                              context,
                                              FoodInformationPage(
                                                navigationWidget:
                                                    FavouritesPage(),
                                              ));
                                        },
                                        child: Container(
                                          height: screenHeight(context,
                                              dividedBy: 2.4),
                                          width: screenWidth(context,
                                              dividedBy: 2),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/gelato.jpg"),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              FavouritesBox(
                                                foodName: "Biriyani",
                                                price: "19",
                                                foodType: "Non-Veg",
                                                hotelName: "Kozhikode",
                                                rating: 19,
                                              ),
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 60),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 30),
                                    )
                                  ],
                                )
                              : Container();
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: 6,
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return index % 2 == 0
                              ? Container()
                              : Column(
                                  children: [
                                    index == 1
                                        ? SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 10),
                                          )
                                        : Container(),
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: GestureDetector(
                                        onTap: () {
                                          push(
                                              context,
                                              FoodInformationPage(
                                                navigationWidget:
                                                    FavouritesPage(),
                                              ));
                                        },
                                        child: Container(
                                          height: screenHeight(context,
                                              dividedBy: 2.4),
                                          width: screenWidth(context,
                                              dividedBy: 2),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/gelato.jpg"),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              FavouritesBox(
                                                foodName: "Biriyani \nmandhi",
                                                price: "19",
                                                foodType: "Non-Veg",
                                                hotelName: "Kozhikode",
                                                rating: 19,
                                              ),
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 60),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 30),
                                    )
                                  ],
                                );
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
