import 'package:app_template/src/ui/screens/email_entering_page.dart';
import 'package:app_template/src/ui/screens/otp_page.dart';
import 'package:app_template/src/ui/screens/tinder_card.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/form_field_password.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          decoration: BoxDecoration(
            color: Colors.black,
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 7),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Image(
                image: AssetImage(
                  "assets/images/logo.png",
                ),
                color: Constants.kitGradients[1],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
            FormFeildUserDetails(
              labelText: "Email Address",
              textEditingController: emailTextEditingController,
            ),
            FormFeildPasswordBox(
              hintText: "Password",
              textEditingController: passwordTextEditingController,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    push(context, EmailEnteringPagePage());
                  },
                  child: Text(
                    "Forget Password?",
                    style: TextStyle(
                        color: Constants.kitGradients[1],
                        fontFamily: 'OpenSansLight',
                        fontSize: 14,
                        decoration: TextDecoration.underline),
                  ),
                ),
                SizedBox(
                    width: screenWidth(
                  context,
                  dividedBy: 10,
                )),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 7),
            ),
            BuildButton(
              title: "Login",
              disabled: false,
              isLoading: isLoading,
              onPressed: () {
                push(context, TinderCard());
                if ((emailTextEditingController.text.trim().length > 0) &&
                    (passwordTextEditingController.text.trim().length > 0)) {
                  final bool isValid =
                      EmailValidator.validate(emailTextEditingController.text);
                  if (isValid) {
                    setState(() {
                      isLoading = true;
                    });
                    push(context, TinderCard());
                  } else {
                    showToasts("please enter a valid email!");
                  }
                } else if ((emailTextEditingController.text.trim().length ==
                        0) &&
                    (passwordTextEditingController.text.trim().length == 0)) {
                  showToasts("please enter email and password");
                } else if (emailTextEditingController.text.trim().length == 0) {
                  showToasts("please enter email");
                } else if (passwordTextEditingController.text.trim().length ==
                    0) {
                  showToasts("please enter password");
                }
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  push(context, EmailEnteringPagePage());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Don't have an account yet?",
                      style: TextStyle(
                        color: Constants.kitGradients[1],
                        fontFamily: 'OpenSansLight',
                        fontSize: screenWidth(context, dividedBy: 22),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        push(context, OTPInputPage());
                      },
                      child: Text(
                        "Sign up here",
                        style: TextStyle(
                            color: Constants.kitGradients[1],
                            fontFamily: 'OpenSansLight',
                            fontSize: screenWidth(context, dividedBy: 22),
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
