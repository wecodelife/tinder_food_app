import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildPasswordBox extends StatefulWidget {
  String hintText;
  TextEditingController textEditingController;
  FormFeildPasswordBox({this.hintText, this.textEditingController});
  @override
  _FormFeildPasswordBoxState createState() => _FormFeildPasswordBoxState();
}

class _FormFeildPasswordBoxState extends State<FormFeildPasswordBox> {
  bool visibility;
  @override
  void initState() {
    visibility = true;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 10),
            vertical: screenHeight(context, dividedBy: 60)),
        child: TextField(
          controller: widget.textEditingController,
          style: TextStyle(
              color: Constants.kitGradients[1],
              fontFamily: 'MulSemiBold',
              fontSize: 16),
          autofocus: false,
          obscureText: visibility,
          cursorColor: Constants.kitGradients[1],
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[1]),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[1]),
              ),
              labelText: widget.hintText,
              labelStyle: TextStyle(
                color: Constants.kitGradients[1],
                fontFamily: 'MuliSemiBold',
                fontSize: 16,
              ),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    visibility = !visibility;
                    print(visibility);
                  });
                },
                child: visibility == false
                    ? Icon(
                        Icons.visibility_off_outlined,
                        color: Colors.grey,
                      )
                    : Icon(
                        Icons.visibility_off_outlined,
                        color: Colors.grey,
                      ),
              )),
        ),
      ),
    );
  }
}
