import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildUserDetails extends StatefulWidget {
  final String labelText;
  final bool readOnly;
  final Function onPressed;

  final TextEditingController textEditingController;
  final ValueChanged onValueChanged;
  FormFeildUserDetails(
      {this.labelText,
      this.onValueChanged,
      this.textEditingController,
      this.onPressed,
      this.readOnly});
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 10)),
      child: TextField(
        controller: widget.textEditingController,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onTap: widget.onPressed,
        onChanged: (e) {
          widget.onValueChanged(widget.textEditingController.text);
          print(widget.textEditingController.text);
        },
        style: TextStyle(
          color: Constants.kitGradients[1],
          fontFamily: 'MuliSemiBold',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[1]),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[1]),
          ),
          labelText: widget.labelText,
          labelStyle: TextStyle(
            color: Constants.kitGradients[1],
            fontFamily: 'MuliSemiBold',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
