import 'package:app_template/src/utils/utils.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FabCircularMenuWidget extends StatefulWidget {
  @override
  _FabCircularMenuWidgetState createState() => _FabCircularMenuWidgetState();
}

class _FabCircularMenuWidgetState extends State<FabCircularMenuWidget> {
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return FabCircularMenu(
      key: fabKey,
      // Cannot be `Alignment.center`
      alignment: Alignment.center,
      ringColor: Colors.white,
      ringDiameter: 300,
      ringWidth: 100,
      fabSize: 64,
      fabElevation: 8.0,
      fabIconBorder: CircleBorder(),
      // Also can use specific color based on wether
      // the menu is open or not:
      // fabOpenColor: Colors.white
      // fabCloseColor: Colors.white
      // These properties take precedence over fabColor

      fabOpenIcon: Icon(Icons.menu, color: Colors.red),
      fabCloseIcon: Icon(Icons.close, color: Colors.red),
      fabMargin: const EdgeInsets.all(20),
      animationDuration: const Duration(milliseconds: 300),
      animationCurve: Curves.bounceOut,
      onDisplayChange: (isOpen) {},
      fabOpenColor: Colors.white,
      children: <Widget>[
        RawMaterialButton(
          onPressed: () {},
          shape: CircleBorder(),
          padding: const EdgeInsets.all(24),
          child: Icon(Icons.looks_3, color: Colors.red),
        ),
        RawMaterialButton(
          onPressed: () {
            fabKey.currentState.close();
          },
          shape: CircleBorder(),
          padding: const EdgeInsets.all(24.0),
          child: Icon(Icons.looks_4, color: Colors.red),
        ),
        RawMaterialButton(
          onPressed: () {},
          shape: CircleBorder(),
          padding: EdgeInsets.all(24),
          child: Icon(Icons.add_circle, color: Colors.red),
        ),
        RawMaterialButton(
          onPressed: () {},
          shape: CircleBorder(),
          padding: EdgeInsets.all(24),
          child: Icon(Icons.add_circle, color: Colors.red),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 10),
        ),
      ],
    );
  }
}
