import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppBarFoodApp extends StatefulWidget {
  final String title;
  final bool leftIconTrue;
  final bool rightIconTrue;
  final IconData rightIcon;
  final IconData leftIcon;
  final Function onPressedLeftIcon;
  final Function onPressedRightIcon;
  AppBarFoodApp(
      {this.title,
      this.leftIcon,
      this.onPressedLeftIcon,
      this.onPressedRightIcon,
      this.rightIcon,
      this.leftIconTrue,
      this.rightIconTrue});
  @override
  _AppBarFoodAppState createState() => _AppBarFoodAppState();
}

class _AppBarFoodAppState extends State<AppBarFoodApp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          widget.leftIconTrue == true
              ? GestureDetector(
                  onTap: () {
                    widget.onPressedLeftIcon();
                  },
                  child: Icon(
                    widget.leftIcon,
                    size: 30,
                  ),
                )
              : SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
          Spacer(
            flex: 2,
          ),
          Container(
            width: screenWidth(context, dividedBy: 2.3),
            child: Center(
              child: Text(
                widget.title,
                style:
                    TextStyle(color: Constants.kitGradients[1], fontSize: 20),
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
          widget.rightIconTrue == true
              ? GestureDetector(
                  onTap: () {
                    widget.onPressedRightIcon();
                  },
                  child: Icon(widget.rightIcon))
              : SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          )
        ],
      ),
    );
  }
}
