import 'package:app_template/src/ui/screens/otp_page.dart';
import 'package:app_template/src/ui/widgets/app_bar.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/form_field_password.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [
          AppBarFoodApp(
            title: "SignUp",
            leftIconTrue: true,
            leftIcon: Icons.arrow_back,
          )
        ],
      ),
      body: Column(
        children: [
          Container(
            height: screenHeight(context, dividedBy: 1.3),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Spacer(flex: 8),
                FormFeildUserDetails(
                  textEditingController: nameTextEditingController,
                  labelText: "UserName",
                  onPressed: () {},
                ),
                Spacer(
                  flex: 1,
                ),
                FormFeildUserDetails(
                  textEditingController: emailTextEditingController,
                  labelText: "Email",
                  onPressed: () {},
                ),
                Spacer(
                  flex: 1,
                ),
                FormFeildPasswordBox(
                  textEditingController: passwordTextEditingController,
                  hintText: "Password",
                ),
                Spacer(flex: 8),
                Spacer(
                  flex: 3,
                )
              ],
            ),
          ),
          BuildButton(
            title: "SignUp",
            disabled: false,
            onPressed: () {
              push(context, OTPInputPage());
            },
          ),
        ],
      ),
    );
  }
}
