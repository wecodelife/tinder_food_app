import 'dart:ui';

import 'package:app_template/src/ui/widgets/price_rating_bar.dart';
import 'package:app_template/src/ui/widgets/rating_bar.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FoodDetailsBox extends StatefulWidget {
  @override
  _FoodDetailsBoxState createState() => _FoodDetailsBoxState();
}

class _FoodDetailsBoxState extends State<FoodDetailsBox> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 20.0,
      left: 10.0,
      right: 10.0,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Container(
            padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
            height: screenHeight(context, dividedBy: 7),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Biriyani",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "rahmath",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white60,
                      ),
                    ),
                    SizedBox(width: 5.0),
                    Container(
                      height: 4.0,
                      width: 4.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white60,
                      ),
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "Kozhikodan",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white60,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    RatingBar(
                      rating: 12,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    SizedBox(width: 5.0),
                    Container(
                      height: 4.0,
                      width: 4.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white60,
                      ),
                    ),
                    SizedBox(width: 5.0),
                    PriceRatingBar(
                      price: 15,
                      size: 20.0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
