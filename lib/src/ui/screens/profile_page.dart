import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  UserBloc userBloc = UserBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blue,
        body: SingleChildScrollView(
          child: Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              child: Stack(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 3.2),
                    color: Constants.kitGradients[1],
                  ),
                  // CAppBar(
                  //     title: "Profile",
                  //     color: Color(0xffD9E7FD),
                  //     isWhite: false),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 6.3),
                    left: screenWidth(context, dividedBy: 6),
                    right: screenWidth(context, dividedBy: 6),
                    child: Container(
                      width: screenWidth(context, dividedBy: 2),
                      height: screenHeight(context, dividedBy: 3.5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 20),
                        //border: Border(top: BorderSide(color:Colors.green, width: 10), left: BorderSide(color:Colors.white, width: 10), right:BorderSide(color:Colors.white, width: 10)),
                        color: Colors.black,
                        shape: BoxShape.circle,
                        //borderRadius: BorderRadius.only(topLeft: Radius.circular((screenWidth(context, dividedBy: 2))/2), topRight: Radius.circular((screenWidth(context, dividedBy: 2))/2))
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 3.3),
                    child: Container(
                      height: screenHeight(context, dividedBy: 1),
                      width: screenWidth(context, dividedBy: 1),
                      color: Colors.white,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 2),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        //color: Colors.blue,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 8),
                            ),
                            Text(
                              "FULL NAME",
                              style: TextStyle(
                                  color: Constants.kitGradients[1],
                                  fontSize: 12,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Text(
                              "Brandon Jake",
                              style: TextStyle(
                                  color: Color(0xff2C2E39),
                                  fontSize: 15,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            Text(
                              "EMAIL",
                              style: TextStyle(
                                  color: Constants.kitGradients[1],
                                  fontSize: 12,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Text(
                              "Brandon@example.com",
                              style: TextStyle(
                                  color: Color(0xff2C2E39),
                                  fontSize: 15,
                                  fontFamily: "SofiaProRegular",
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 4.5),
                            ),
                            BuildButton(
                              onPressed: () {},
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  new Positioned(
                    top: screenHeight(context, dividedBy: 4.6),
                    left: screenWidth(context, dividedBy: 7),
                    right: screenWidth(context, dividedBy: 7),
                    child: Container(
                      width: screenWidth(context, dividedBy: 4),
                      height: screenHeight(context, dividedBy: 6),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Constants.kitGradients[1], width: 3),
                        //color:Colors.yellow,
                        shape: BoxShape.circle,
                      ),
                      child: CircleAvatar(
                        backgroundColor: Color(0xff4385F5),
                        backgroundImage: NetworkImage(
                            "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"),
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
