import 'package:app_template/src/ui/widgets/app_bar.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_field_password.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController reEnterPasswordTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [
          AppBarFoodApp(
            title: "Forgot Password",
            leftIconTrue: true,
            leftIcon: Icons.arrow_back,
          )
        ],
      ),
      body: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 5),
          ),
          FormFeildPasswordBox(
            textEditingController: passwordTextEditingController,
            hintText: "NewPassword",
          ),
          SizedBox(
            height: 10,
          ),
          FormFeildPasswordBox(
            textEditingController: reEnterPasswordTextEditingController,
            hintText: "Re-EnterPassword",
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3),
          ),
          BuildButton(
            title: "NEXT",
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
