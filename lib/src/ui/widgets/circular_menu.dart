import 'package:app_template/src/ui/screens/favourites_page.dart';
import 'package:app_template/src/ui/screens/food_information_page.dart';
import 'package:app_template/src/ui/screens/tinder_card.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:circular_menu/circular_menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircularMenuWidget extends StatefulWidget {
  @override
  _CircularMenuWidgetState createState() => _CircularMenuWidgetState();
}

class _CircularMenuWidgetState extends State<CircularMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return CircularMenu(
      startingAngleInRadian: 600,
      endingAngleInRadian: 250,
      alignment: Alignment.center,
      reverseCurve: Curves.decelerate,
      toggleButtonColor: Constants.kitGradients[1],
      radius: screenHeight(context, dividedBy: 8),
      animationDuration: Duration(milliseconds: 200),
      curve: Curves.bounceOut,
      toggleButtonSize: 30,
      items: [
        CircularMenuItem(
            icon: Icons.search,
            color: Colors.green,
            onTap: () {
              print(3);
            }),
        CircularMenuItem(
            icon: Icons.menu,
            color: Colors.blue,
            onTap: () {
              print(2);
              pushAndReplacement(
                  context,
                  FoodInformationPage(
                    navigationWidget: TinderCard(),
                  ));
            }),
        CircularMenuItem(
            icon: Icons.thumb_up,
            color: Colors.orange,
            onTap: () {
              print(1);
              pushAndReplacement(
                context,
                FavouritesPage(),
              );
            }),
      ],
    );
  }
}
