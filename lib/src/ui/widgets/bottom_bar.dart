import 'package:app_template/src/ui/widgets/matches.dart';
import 'package:app_template/src/ui/widgets/tinder_swap_cards.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatefulWidget {
  final MatchEngine matchEngine;
  final Function onPressedBack;
  BottomBar({this.matchEngine, this.onPressedBack});
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 10),
      width: screenWidth(context, dividedBy: 1),
      child: new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new RoundIconButton.small(
                icon: Icons.refresh,
                iconColor: Colors.orange,
                onPressed: () {
                  // widget.matchEngine.currentMatch.nope();
                  widget.matchEngine.prevImage();
                }),
            Spacer(
              flex: 1,
            )

            // new RoundIconButton.large(
            //   icon: Icons.clear,
            //   iconColor: Colors.red,
            //   onPressed: () {
            //     widget.matchEngine.currentMatch.nope();
            //     widget.matchEngine.cycleMatch();
            //   },

            // new RoundIconButton.small(
            //   icon: Icons.star,
            //   iconColor: Colors.blue,
            //   onPressed: () {
            //     widget.matchEngine.currentMatch.superLike();
            //     widget.matchEngine.cycleMatch();
            //   },
            // ),
            // new RoundIconButton.large(
            //   icon: Icons.favorite,
            //   iconColor: Colors.green,
            //   onPressed: () {
            //     widget.matchEngine.currentMatch.like();
            //     widget.matchEngine.cycleMatch();
            //   },
            // ),
            // new RoundIconButton.small(
            //   icon: Icons.lock,
            //   iconColor: Colors.purple,
            //   onPressed: () {},
            // ),
          ],
        ),
      ),
    );
  }
}
