import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundFloatingActionButton extends StatefulWidget {
  final double height;
  final String img;
  final double imageHeight;
  final double imageWidth;
  final Function onPressed;

  RoundFloatingActionButton(
      {this.height,
      this.imageHeight,
      this.imageWidth,
      this.img,
      this.onPressed});

  @override
  _RoundFloatingActionButtonState createState() =>
      _RoundFloatingActionButtonState();
}

class _RoundFloatingActionButtonState extends State<RoundFloatingActionButton> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Colors.white,
      elevation: 4.0,
      onPressed: widget.onPressed,
      height: screenHeight(context, dividedBy: widget.height),
      shape: CircleBorder(),
      child: Container(
        height: 40.0,
        child: Image.asset(
          widget.img,
          height: screenHeight(context, dividedBy: widget.imageHeight),
          width: screenWidth(context, dividedBy: widget.imageWidth),
        ),
      ),
    );
    ;
  }
}
