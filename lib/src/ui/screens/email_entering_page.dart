import 'package:app_template/src/ui/screens/signup_page.dart';
import 'package:app_template/src/ui/widgets/app_bar.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmailEnteringPagePage extends StatefulWidget {
  @override
  _EmailEnteringPagePageState createState() => _EmailEnteringPagePageState();
}

class _EmailEnteringPagePageState extends State<EmailEnteringPagePage> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [
          AppBarFoodApp(
            title: "EMAIL",
            leftIconTrue: true,
            leftIcon: Icons.arrow_back,
            onPressedLeftIcon: () {
              pop(context);
            },
          )
        ],
      ),
      body: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 2.9),
          ),
          SizedBox(
            height: 10,
          ),
          FormFeildUserDetails(
            textEditingController: emailTextEditingController,
            labelText: "Email",
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3),
          ),
          BuildButton(
            title: "NEXT",
            disabled: false,
            onPressed: () {
              if (emailTextEditingController.text != null) {
                push(context, SignUpPage());
              } else {
                showToasts("Enter Your Email");
              }
            },
          )
        ],
      ),
    );
  }
}
