import 'package:app_template/src/ui/widgets/card_stack.dart';
import 'package:app_template/src/ui/widgets/matches.dart';
import 'package:flutter/material.dart';

class TinderSwapCard extends StatefulWidget {
  TinderSwapCard({
    Key key,
    this.title,
    this.demoProfiles,
    this.myCallback,
  }) : super(key: key);

  final String title;
  final List demoProfiles;

  final Function(Decision) myCallback;

  @override
  _TinderSwapCardState createState() => _TinderSwapCardState();
}

class _TinderSwapCardState extends State<TinderSwapCard> {
  Match match = new Match();

  @override
  Widget build(BuildContext context) {
    final MatchEngine matchEngine = new MatchEngine(
        matches: widget.demoProfiles.map((final profile) {
      return Match(profile: profile);
    }).toList());

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.zero,
        child: new CardStack(
            matchEngine: matchEngine,
            onSwipeCallback: (match) {
              widget.myCallback(match);
            }),
      ),
    );
  }
}

class RoundIconButton extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final double size;
  final VoidCallback onPressed;

  RoundIconButton.large({
    this.icon,
    this.iconColor,
    this.onPressed,
  }) : size = 60.0;

  RoundIconButton.small({
    this.icon,
    this.iconColor,
    this.onPressed,
  }) : size = 50.0;

  RoundIconButton({
    this.icon,
    this.iconColor,
    this.size,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            new BoxShadow(color: const Color(0x11000000), blurRadius: 10.0),
          ]),
      child: new RawMaterialButton(
        shape: new CircleBorder(),
        elevation: 0.0,
        child: new Icon(
          icon,
          color: iconColor,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
