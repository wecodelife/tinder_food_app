import 'dart:ui';

import 'package:app_template/src/ui/widgets/price_rating_bar.dart';
import 'package:app_template/src/ui/widgets/rating_bar.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavouritesBox extends StatefulWidget {
  final String foodName;
  final String hotelName;
  final String foodType;
  final double rating;
  final String price;
  FavouritesBox(
      {this.rating, this.price, this.foodType, this.hotelName, this.foodName});

  @override
  _FavouritesBoxState createState() => _FavouritesBoxState();
}

class _FavouritesBoxState extends State<FavouritesBox> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
      child: Container(
        width: screenWidth(context, dividedBy: 2.3),
        height: screenHeight(context, dividedBy: 7),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Padding(
            padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 60)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.foodName,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      widget.hotelName,
                      style: TextStyle(
                        fontSize: 12.0,
                        color: Colors.white60,
                      ),
                    ),
                    SizedBox(width: 5.0),
                    _filledCircle,
                    SizedBox(width: 5.0),
                    Text(
                      widget.foodType,
                      style: TextStyle(
                        fontSize: 12.0,
                        color: Colors.white60,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    RatingBar(rating: widget.rating, color: Colors.white),
                    SizedBox(width: 5.0),
                    _filledCircle,
                    SizedBox(width: 5.0),
                    PriceRatingBar(
                      price: int.parse(widget.price),
                      size: 16,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  final _filledCircle = Container(
    height: 4.0,
    width: 4.0,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.white60,
    ),
  );
}
