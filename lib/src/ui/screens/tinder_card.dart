import 'package:app_template/src/ui/widgets/tinder_swap_cards.dart';
import 'package:flutter/material.dart';

class TinderCard extends StatefulWidget {
  @override
  _TinderCardState createState() => _TinderCardState();
}

class _TinderCardState extends State<TinderCard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TinderSwapCard(
        demoProfiles: demoProfiles,
        myCallback: (decision) {},
      ),
    );
  }

  List<Profile> demoProfiles = [
    new Profile(
      photos: [
        "assets/images/woman.jpg",
        "assets/images/woman.jpg",
        "assets/images/woman.jpg",
      ],
      name: "Aneesh G",
      bio: "This is the person you want",
    ),
    new Profile(
      photos: [
        "assets/images/cafe.jpg",
        "assets/images/cafe.jpg",
        "assets/images/cafe.jpg",
        "assets/images/cafe.jpg",
      ],
      name: "Amanda Tylor",
      bio: "You better swpe left",
    ),
    new Profile(
      photos: [
        "assets/images/gelato.jpg",
        "assets/images/gelato.jpg",
        "assets/images/gelato.jpg",
        "assets/images/gelato.jpg",
      ],
      name: "Godson Mathew",
      bio: "You better swpe left",
    ),
    new Profile(
      photos: [
        "assets/images/tacos.jpg",
        "assets/images/tacos.jpg",
        "assets/images/tacos.jpg",
        "assets/images/tacos.jpg",
        "assets/images/tacos.jpg",
      ],
      name: "Godson Mathew",
      bio: "You better swpe left",
    ),
  ];
}

class Profile {
  final List<String> photos;
  final String name;
  final String bio;

  Profile({this.photos, this.name, this.bio});
}

//dummy data
